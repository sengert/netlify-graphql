const { ApolloServer, gql } = require("apollo-server-lambda");

const pages = [
    {
        id: '93029',
        metaTitle: 'Homepage',
        metaDescription: 'Wecome to the site',
        slug: '/',
        blocks: ['902888', '909945'],
    },
    {
        id: '234765',
        metaTitle: 'Subpage',
        metaDescription: 'this is a subpage',
        slug: '/somepage',
        blocks: ['902348', '309459']
    }
];

const blocks = [
    {
        id: '909945',
        type: 'SubscribeBlock',
    },
    {
        id: '902348',
        type: 'SubscribeBlock',
    },
    {
        id: '902888',
        type: 'HeroBlock',
        featuredImage: 'https://picsum.photos/id/451/200/300',
        title: 'Motel',
        subtitle: 'in the sky'
    },
    {
        id: '309459',
        type: 'HeroBlock',
        featuredImage: 'https://picsum.photos/id/551/200/300',
        title: 'Dusty Car',
    }
]


const typeDefs = gql`
    type Query {
        blocks: [Block]
        page(slug: String!): Page!
        block(id: ID!): Block!
    }

    type Page {
        id: ID!,
        metaTitle: String!,
        metaDescription: String!,
        slug: String!,
        blocks: [Block]!,
    }

    union Block = HeroBlock | SubscribeBlock

    type HeroBlock {
        id: ID!,
        type: String!,
        featuredImage: String!,
        title: String!,
        subtitle: String,
    }
    type SubscribeBlock {
        id: ID!,
        type: String!
    }
`;

const resolvers = {
    Query: {
        blocks: (parent, args, ctx, info) =>  {
            return blocks;
        },
        page: (parent, args, ctx, info) => {
            return pages.find(page => page.slug === args.slug);
        },
        block (parent, args, ctz, info) {
            return blocks.find(block => block.id === args.id);
        }
    },
    Page: {
        blocks(parent, args, ctx, info) {
            return parent.blocks.map(blockId => blocks.find(({id}) => blockId === id));
        }
    },
    Block: {
        __resolveType(obj, ctx, info) {
            if (obj.type === 'HeroBlock') {
                return 'HeroBlock';
            } else if (obj.type === 'SubscribeBlock') {
                return 'SubscribeBlock';
            }
            return null;
        }
    }
};

const server = new ApolloServer({
    typeDefs,
    resolvers
});

const graphqlHandler = server.createHandler();

exports.handler = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    function callbackFilter(error, output) {
      output.headers['Access-Control-Allow-Origin'] = '*';
      output.headers['Access-Control-Allow-Headers'] = '*';
      callback(error, output);
    }
    graphqlHandler(event, context, callbackFilter);
};;